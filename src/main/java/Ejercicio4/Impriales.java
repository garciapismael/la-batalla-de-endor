package Ejercicio4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Impriales implements Runnable{
    List<Thread> cazasList = new ArrayList<>();

    public Impriales(Thread[] cazasList) {
        for(Thread cazas: cazasList){
            this.cazasList.add(cazas);
        }
    }
    @Override
    public void run() {
        for(int i = 0; i < cazasList.size(); i++){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean matarCaza = ThreadLocalRandom.current().nextInt(0, 100) < 10;

            if (matarCaza) {
                Thread t = cazasList.get(0);
                if(t.isAlive()){
                    t.interrupt();
                    cazasList.remove(t);
                    System.out.println("Un imperial a matado a un caza");
                }
            }
        }
    }
}
