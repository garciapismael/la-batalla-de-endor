package Ejercicio1;

public class Ejercicio1 {
    private static int maxDistancia = 20000;
    private static int distanciaPorSegundo= 500;

    public static void main(String[] args) {
        Thread[] cazas = new Thread[10];
        for (int i = 0; i < cazas.length; i++) {
            cazas[i] = new Thread(new Cazas(i, maxDistancia, distanciaPorSegundo));
        }

        for (int i = 0; i < cazas.length; i++) {
            cazas[i].start();
        }

        for (int i = 0; i < cazas.length; i++) {
            try {
                cazas[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

}
