package Ejercicio1;

import java.util.concurrent.ThreadLocalRandom;

public class Cazas implements Runnable{
    private int id;
    private int maxDistancia;
    private int distanciaRecorida;
    private int distanciaPorSegundo;
    private int cazasMataos = 0;

    public Cazas(int id, int maxDistancia, int distanciaPorSegundo) {
        this.id = id;
        this.maxDistancia = maxDistancia;
        this.distanciaPorSegundo = distanciaPorSegundo;
    }


    @Override
    public void run() {
        while (maxDistancia > 0){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            maxDistancia-=distanciaPorSegundo;
            distanciaRecorida += distanciaPorSegundo;
            boolean matarEnemigo = ThreadLocalRandom.current().nextInt(0, 100) < 80;
            if(matarEnemigo){
                System.out.println("A acabado con un caza");
                System.out.println("El X-Wing" + id + " distancia recorida: " + distanciaRecorida + ". A acabado con un caza");
                cazasMataos++;
            }else{
                System.out.println("El X-Wing" + id + " distancia recorida: " + distanciaRecorida);
            }

        }
        System.out.println("El X-Wing" + id + " distancia recorida: " + distanciaRecorida + " A acabado con: " + cazasMataos + " cazas");
    }

    public int getCazasMataos() {
        return cazasMataos;
    }
}
