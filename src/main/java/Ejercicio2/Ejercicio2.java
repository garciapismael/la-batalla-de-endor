package Ejercicio2;

import Ejercicio2.Cazas;

public class Ejercicio2 {
    private static int maxDistancia = 20000;
    private static int distanciaPorSegundo= 500;

    public static void main(String[] args) {
        Thread[] cazas = new Thread[10];
        Escuadron escuadron = new Escuadron();
        for (int i = 0; i < cazas.length; i++) {
            cazas[i] = new Thread(new Cazas(i, maxDistancia, distanciaPorSegundo, escuadron));
        }

        for (int i = 0; i < cazas.length; i++) {
            cazas[i].start();
        }

        for (int i = 0; i < cazas.length; i++) {
            try {
                cazas[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Total cazas matados " + escuadron.getTotalCazasMatados());
    }
}
