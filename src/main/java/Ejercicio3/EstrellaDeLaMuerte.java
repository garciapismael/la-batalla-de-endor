package Ejercicio3;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class EstrellaDeLaMuerte implements Runnable{
    List<Thread> cazasList = new ArrayList<>();

    public EstrellaDeLaMuerte(Thread[] cazasList) {
        for(Thread cazas: cazasList){
            this.cazasList.add(cazas);
        }
    }


    @Override
    public void run() {
        for(int i = 0; i < cazasList.size(); i++){
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean matarCaza = ThreadLocalRandom.current().nextInt(0, 100) < 70;

            if (matarCaza) {
                Thread t = cazasList.get(0);
                if(t.isAlive()){
                    t.interrupt();
                    cazasList.remove(t);
                    System.out.println("A matado a un caza");
                }
            }
        }
    }
}
