package Ejercicio3;

import java.util.concurrent.ThreadLocalRandom;

public class Cazas implements Runnable{
    private int id;
    private int maxDistancia;
    private int distanciaRecorida;
    private int distanciaPorSegundo;
    private int cazasMataos = 0;
    private Escuadron escuadron;
    private boolean misionCompletada = false;

    public Cazas(int id, int maxDistancia, int distanciaPorSegundo, Escuadron escuadron) {
        this.id = id;
        this.maxDistancia = maxDistancia;
        this.distanciaPorSegundo = distanciaPorSegundo;
        this.escuadron = escuadron;
    }


    @Override
    public void run() {
        try {
        while (maxDistancia > 0){

                Thread.sleep(1000);

            maxDistancia-=distanciaPorSegundo;
            distanciaRecorida += distanciaPorSegundo;
            boolean matarEnemigo = ThreadLocalRandom.current().nextInt(0, 100) < 80;
            if(matarEnemigo){
                System.out.println("A acabado con un caza");
                System.out.println("El X-Wing" + id + " distancia recorida: " + distanciaRecorida + "A acabado con un caza");
                cazasMataos++;
                escuadron.sumarCaza();
            }else{
                System.out.println("El X-Wing" + id + " distancia recorida: " + distanciaRecorida);
            }

        }
        System.out.println("El X-Wing" + id + " distancia recorida: " + distanciaRecorida + " A acabado con: " + cazasMataos + " cazas");
        if(maxDistancia == 0){
            misionCompletada = true;
        }
        } catch (InterruptedException e) {
            System.out.println("Caza " + id + "Eliminado");
        }
    }

    public int getCazasMataos() {
        return cazasMataos;
    }

    public boolean isMisionCompletada() {
        return misionCompletada;
    }
}
