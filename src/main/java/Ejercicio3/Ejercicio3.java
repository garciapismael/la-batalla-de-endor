package Ejercicio3;

public class Ejercicio3 {
    private static int maxDistancia = 20000;
    private static int distanciaPorSegundo= 500;
    private static boolean bol = true;

    public static void main(String[] args) {
        Cazas[] cazas = new Cazas[10];
        Thread[] threads = new Thread[cazas.length];
       Escuadron escuadron = new Escuadron();
        for (int i = 0; i < threads.length; i++) {
            cazas[i] = new Cazas(i, maxDistancia, distanciaPorSegundo, escuadron);
            threads[i] = new Thread(cazas[i]);
        }
        EstrellaDeLaMuerte estrellaDeLaMuerte = new EstrellaDeLaMuerte(threads);
        Thread t = new Thread(estrellaDeLaMuerte);
        t.start();
        for (int i = 0; i < threads.length; i++) {
            threads[i].start();
        }

        for (int i = 0; i < threads.length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Total cazas matados " + escuadron.getTotalCazasMatados());
        for (int i = 0; i < threads.length; i++){

            if(cazas[i].isMisionCompletada()){
                System.out.println("Un Caza a llegado, mision completada");
                bol = true;
                break;
            }
        }
        if(!bol){
            System.out.println("La mision a fracasado");

        }

    }
}
