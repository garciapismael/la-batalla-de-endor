package Ejercicio3;

public class Escuadron {
    private int totalCazasMatados = 0;

    public Escuadron() {
    }

    public void sumarCaza(){
        totalCazasMatados++;
    }

    public int getTotalCazasMatados() {
        return totalCazasMatados;
    }
}
